var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,passwd) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal ,
    pass: passwd
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  //var patt = /" "/;
 // var tmpBesede = ukaz.split(patt);
  var besede = ukaz.split(' ');
  //var besede = ukaz.split(/ +/); <-- dodatno
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var pass='!_.1-';
     // var rez=kanal.split(' ');
      var kanal = besede.join(' ');

      var re = /".*" *".*"/;
      var rez=re.test(kanal);
      if(rez){
        var loceno=kanal.split(/" *"/);
        kanal=loceno[0].substring(1,loceno[0].length);
        pass=loceno[1].substring(0,loceno[1].length-1);
      }
      this.spremeniKanal(kanal, pass);
     // this.socket.emit('izpis', {sporocilo : kanal+" :"+pass+" l:"+rez}); testno izpisovanje
      break; 

    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
    // var message = { ime : "ime", besedilo : "nekej" };
     //besede.shift();
      var patt = /" *"/;
      besede.shift();
      var ime = besede.join(' ');
      if((/^".*" *".*"$/).test(ime)){
        var tmpBesede= ime.split(patt);
        var ime1=tmpBesede[0].substring(1, tmpBesede[0].length);
        var besedilo=tmpBesede[1].substring(0, tmpBesede[1].length-1);
        //besede.shift();
        //var vzdevek = besede.join(' ');
        this.socket.emit('zasebnoSporocilo',{ime : ime1 , besedilo : besedilo}); ////<--- to trenutno delujem
      }else sporocilo = 'Neznan ukaz.';
   
      break;
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};