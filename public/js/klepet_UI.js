//var strez = require("../streznik.js"); <--txt datoteka ne dela 
//var predPomnilnik=strez.dejP();
//var xmlHttpReq;
//2. nacin z xml:
//if (window.XMLHttpRequest){

var xmlDoc;
var  xmlHttpReq=new XMLHttpRequest();
//}else xmlHttpReq=new ActiveXObject("Microsoft.XMLHTTP"); <-- ne zazna ActiveXObject
xmlHttpReq.open("GET","swearWords.xml",false);
xmlHttpReq.send();
xmlDoc=xmlHttpReq.responseXML;

function replacer(match, p1, p2, p3, offset, string){
  var blank='';
  for(var j=0;j<p2.length;j++){
      blank+='*';
    }

  return [p1, blank, p3].join('');
}


// ###################################################--------------------------------------------
// moje funkcije za zamenjavo sporocila

function zamenjajBesede(sporocilo){
  var listWords=xmlDoc.getElementsByTagName("word");
  //var test = listWords[1].childNodes[0].nodeValue;
 // var listWords1={};
 var i,j;  //zakaj ne dela for (var i in listWords) ??? 
  for(i=0;i<listWords.length;i++){
    //var beseda="alabam";
    var beseda=listWords[i].childNodes[0].nodeValue;
    var blank='';
    
    for(j=0;j<beseda.length;j++){
      blank+='*';
    }
    
    var beseda1='(\\W+)('+beseda+')(\\W+)'; //|(\\b'+beseda+'\\b)';
    var beseda2='(\\b'+beseda+'\\b)';
    var regExp1=new RegExp(beseda1,"gi");
    var regExp2=new RegExp(beseda2,"gi");
    sporocilo=sporocilo.replace(regExp1,replacer);
    sporocilo=sporocilo.replace(regExp2,blank);
  }
  return sporocilo;
}

function zamenjajXSS(besedilo){
    var tmpBesedilo=besedilo.replace(/</g,"&lt");
    tmpBesedilo=tmpBesedilo.replace(/>/g,"&gt");
    return tmpBesedilo;
}
function zamenjajSmeske(besedilo){
  var sporocilo=besedilo.replace(/;\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">");
  sporocilo=sporocilo.replace(/:\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">");
  sporocilo=sporocilo.replace(/\(y\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">");
  sporocilo=sporocilo.replace(/:\*/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">");
  sporocilo=sporocilo.replace(/:\(/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">");
  return sporocilo;
}
// #################################-----------------------

function divElementEnostavniTekst(sporocilo) {

  
  return $('<div style="font-weight: bold"></div>').html(sporocilo); //nisem:13.16 iz text v html
}

function divElementHtmlTekst(sporocilo) {
 // sporocilo=sporocilo.replace(/</g,"&lt");   // DOOODAU To ... 2:17
 // sporocilo=sporocilo.replace(/>/g,"&gt"); // in to
  //sporocilo=zamenjajXSS(sporocilo);
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
    // sporocilo=sporocilo.replace(/</g,"&lt");  /// evo proba to : 2:18 <3če pustim replace() na tem mestu, mi popravi imena, ampak je napaka v kanalih
     // sporocilo=sporocilo.replace(/>/g,"&gt");

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo=zamenjajXSS(sporocilo);
    sporocilo=zamenjajSmeske(sporocilo);
    sporocilo=zamenjajBesede(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
   $('#sporocila').append(divElementEnostavniTekst(sporocilo));// bom preposlal
  // socket.emit()


    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal1)
      //$('#kanal').text(rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    resetirajSeznam(rezultat.users);
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
/*<<<<<<< HEAD

    $('#kanal').text(rezultat.ime +" @ "+rezultat.kanal);  //probavam 
    

    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    resetirajSeznam(rezultat.users);
    
=======
*/
   if(rezultat.uspesno){
   // $('#kanal').text(rezultat.kanal); 2.7
   $('#kanal').text(rezultat.ime +" @ "+rezultat.kanal); //HEAD
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    resetirajSeznam(rezultat.users); //HEAD
   }else  $('#sporocila').append(divElementHtmlTekst(zamenjajXSS(rezultat.sporocilo)));
//>>>>>>> Naloga_2_7

  });

  socket.on('sporocilo', function (sporocilo) {
   // var novSporocilo = sporocilo.besedilo.replace(/</g,"&lt"); //12:35
      //  novSporocilo = novSporocilo.replace(/>/g,"&gt");
     //   novSporocilo=novSporocilo.replace(/;\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">");
   //  novSporocilo=novSporocilo.replace(/:\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">");
  //   novSporocilo=novSporocilo.replace(/\(y\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">");
 //   novSporocilo=novSporocilo.replace(/:\*/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">");
   // novSporocilo=novSporocilo.replace(/:\(/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">");
    //original//var novElement = $('<div style="font-weight: bold"></div>').html(novSporocilo); //12:35 iz text v html in novSpor<>spor
  //original//  $('#sporocila').append(novElement);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));  //dodano 13.15
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
       // $('#seznam-kanalov').append(divElementEnostavniTekst(kanal)); probam eno čudo
       $('#seznam-kanalov').append($('<div style="font-weight: bold"></div>').text(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());  //to sm zdej spremenu 2:11 
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('seznam');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });





function resetirajSeznam(seznam){
   $('#seznam-prijavljenih').empty();
/*
    for(var i in uporabniki.users) {
      var vzdevek = uporabniki.ign[uporabniki.users[i]];
*/     
    var users=seznam.split('>!1>-<');
    for (var i in users)
    $('#seznam-prijavljenih').append($('<div style="font-weight: bold"></div>').text(users[i]));
  
} ; 
  socket.on('uporabniki', function(uporabniki) {
    resetirajSeznam(uporabniki.users);
      
  //  }

  });
  


  //dodal jst :
  socket.on('zasSporOdgovor', function(sporocilo){
    //console.log("neki je." +sporocilo.besedilo);
   // $('#sporocila').append(divElementHtmlTekst(sporocilo.besedilo+' ime:'+sporocilo.ime+' oseba:'+sporocilo.oseba));
    var besedilo='';
    if(sporocilo.oseba == 'neuspesno'){
      besedilo = "Sporočila "+zamenjajBesede(sporocilo.besedilo)+" uporabniku z vzdevkom "+sporocilo.ime+" ni bilo mogoče posredovati.";
     // var novElement = $('<div style="font-weight: bold"></div>').text(besedilo);
      $('#sporocila').append(divElementHtmlTekst(besedilo));
      //$('#sporocila').append(divElementHtmlTekst(sporocilo.besedilo));
      
    }else if(sporocilo.oseba == 'sender'){
      besedilo='(zasebno za '+sporocilo.ime+')' + ': ' +zamenjajBesede(sporocilo.besedilo);
      $('#sporocila').append(divElementEnostavniTekst(besedilo));
    }else{// if(sporocilo.oseba == 'reciever'){
      besedilo = sporocilo.ime+' (zasebno)' + ': ' +zamenjajBesede(sporocilo.besedilo);
    $('#sporocila').append(divElementEnostavniTekst(besedilo));
  }});


});

