var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
//backup commit pred glavnim zdruzevanjem

var kanalPasswd = {};
//Kreiranje novega kanala z geslom


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);

    pridruzitevKanalu(socket, 'Skedenj','!_.1-');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket); // dodal
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('seznam',function(){
      socket.emit('uporabniki',{users : ustvariSeznam(socket, trenutniKanal[socket.id])});
      
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    kanal1: trenutniKanal[socket.id] // dodal
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

/*/<<<<<<< HEAD
function pridruzitevKanalu(socket, kanal) {
  
  //ustvariSeznam(socket);
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
<<<<<<< HEAD

  ustvariSeznam(socket,trenutniKanal[socket.id]);
  socket.emit('pridruzitevOdgovor', {
    kanal: kanal,
    users: ustvariSeznam(socket,trenutniKanal[socket.id]),
    ime: vzdevkiGledeNaSocket[socket.id]
=======
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: zamenjajXSS(vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.')
>>>>>>> Naloga_2_1
  });

  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: zamenjajXSS(vzdevkiGledeNaSocket[socket.id]) + ' se je pridružil kanalu ' + zamenjajXSS(kanal) + '.'
  });
  //var users='';
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
//=======
*/
function pridruzitevKanalu(socket, kanal, pass) {
  /*  <---nevem zakaj je to v commentih
  var legalno=true;
  var sporocilo;
  for(var i in trenutniKanal && legalno){
    if(trenutniKanal[i]==kanal && kanalPasswd[i]!=pass){
      if(pass == '!_.1-'){
        sporocilo="Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!";
      }else sporocilo = "Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+kanal+" ali zahtevajte kreiranje kanala z drugim imenom.";
        legalno=false;
        socket.emit('pridruzitevOdgovor',{kanal: kanal , sporocilo : sporocilo, uspesno: false});
    }
  }
  if(legalno){
  */
    socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    kanalPasswd[socket.id] = pass;
    //socket.emit('pridruzitevOdgovor', {kanal: kanal, uspesno : true});  pred mergom 2.7
   // socket.broadcast.to(kanal).emit('sporocilo', {
    //  besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    //});
//HEAD ---    
    ustvariSeznam(socket,trenutniKanal[socket.id]);
  socket.emit('pridruzitevOdgovor', {
    kanal: kanal,
    users: ustvariSeznam(socket,trenutniKanal[socket.id]),
    ime: vzdevkiGledeNaSocket[socket.id],
    uspesno : true
  });

  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: zamenjajXSS(vzdevkiGledeNaSocket[socket.id]) + ' se je pridružil kanalu ' + zamenjajXSS(kanal) + '.'
  });
//--- HEAD
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
//>>>>>>> Naloga_2_7
        }
      }

      //dodajanje uporabnikov v string za posiljanje naprej
    //  users+= vzdevkiGledeNaSocket[uporabnikSocketId] +' ';
      uporabnikiNaKanaluPovzetek += '.';
   // uporabnikiNaKanaluPovzetek='dela';
      socket.emit('sporocilo', {besedilo: zamenjajXSS(uporabnikiNaKanaluPovzetek)});
    }

}
 // socket.broadcast.to(kanal).emit('uporabniki',{users: users});



function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
       // vzdevek=vzdevek.replace(/\)/,"&#41");
       // vzdevek=vzdevek.replace(/\(/,"&#40");  popravi vpise .. ampak v tabeli napake
       // vzdevek=vzdevek.replace(/</g,"&lt");
      //  vzdevek=vzdevek.replace(/>/g,"&gt");
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,

          kanal1: trenutniKanal[socket.id] ,//dodal

          users: ustvariSeznam(socket,trenutniKanal[socket.id])

        });
        ustvariSeznam(socket,trenutniKanal[socket.id]);
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {

          besedilo: zamenjajXSS(prejsnjiVzdevek) + ' se je preimenoval v ' + zamenjajXSS(vzdevek) + '.'

        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {

    //var besedilo = zamenjajXSS(sporocilo.besedilo);
  //  besedilo  = zamenjajSmeske(besedilo);
    socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {  //spremenu iz sporocilo.kanal v trenutniKanal[socket.id]

      besedilo: zamenjajXSS(vzdevkiGledeNaSocket[socket.id]) + ': ' + sporocilo.besedilo
    });
    //console.log(sporocilo.kanal); preveril kaj sploh vrne sporocilo.kanal
  });
}

function obdelajPridruzitevKanalu(socket) {
/*
<<<<<<< HEAD
  socket.on('pridruzitevZahteva', function(kanal) {
   // ustvariSeznam(socket);
    var tmpKanal=trenutniKanal[socket.id];
   
    socket.leave(trenutniKanal[socket.id]);
     ustvariSeznam(socket,tmpKanal);
    pridruzitevKanalu(socket, kanal.novKanal);
=======
*/
  socket.on('pridruzitevZahteva', function(objekt) {
      var kanal=objekt.novKanal;
      var pass = objekt.pass;
      var legalno=true;
      var sporocilo;
      for(var i in trenutniKanal){
        if(trenutniKanal[i]==kanal && kanalPasswd[i]!=pass){
          if(kanalPasswd[i] != '!_.1-'){
            sporocilo="Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!";
          }else sporocilo = "Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+kanal+" ali zahtevajte kreiranje kanala z drugim imenom.";
            legalno=false;
            socket.emit('pridruzitevOdgovor',{kanal: kanal , sporocilo : sporocilo, uspesno: false});
            break;
        }
      }
    if(legalno){  //ce je dovoljeno naredi
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal , pass);
     // pridruzitevKanalu(socket);
    }
// <-----------do tukej <<<<<<<<<<<< 2.7
  });
  /* Izpisujem za testiranje
  socket.on('izpis',function(sporocila){
    socket.emit('izpisOdg',sporocila.sporocilo);
  })
  */
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
   // ustvariSeznam(socket);
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    ustvariSeznam(socket, trenutniKanal[socket.id]);
  });
}



//#########################----------------------------------------- moje kode VVVVV

//funckcija za izpis seznama
function ustvariSeznam(socket, kanal){
 // var kanal = trenutniKanal[socket.id]
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabnikiNaKanaluPovzetek='';
    for (var i in uporabnikiNaKanalu) {
      if(i>0)uporabnikiNaKanaluPovzetek+='>!1>-<';
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
      //dodajanje uporabnikov v string za posiljanje naprej
  
 socket.broadcast.to(kanal).emit('uporabniki',{users: uporabnikiNaKanaluPovzetek});
 return uporabnikiNaKanaluPovzetek;
}


//dodal jst :
function obdelajZasebnoSporocilo(socket){
  socket.on('zasebnoSporocilo', function(sporocilo){
    var sockI;
    var besedilo = sporocilo.besedilo;
    besedilo=zamenjajXSS(besedilo);
    besedilo=zamenjajSmeske(besedilo);
    if(uporabljeniVzdevki.indexOf(sporocilo.ime) == -1 || vzdevkiGledeNaSocket[socket.id]==sporocilo.ime){
        socket.emit('zasSporOdgovor',{
          oseba: 'neuspesno' ,
          besedilo: besedilo,
          ime: sporocilo.ime
          //"Sporočila "+besedilo+" uporabniku z vzdevkom "+sporocilo.ime+" ni bilo mogoče posredovati."
        });
      
    }else{
      for (var x in vzdevkiGledeNaSocket){
        if(vzdevkiGledeNaSocket[x]==sporocilo.ime)
          sockI=x;
      }
      
      //sockId.emit('privat',{besedilo : sporocilo.besedilo});
     io.sockets.socket(sockI).emit('zasSporOdgovor',{
       oseba : 'rec' ,
       besedilo: besedilo,
       ime : zamenjajXSS(vzdevkiGledeNaSocket[socket.id])
     });
     // besedilo :vzdevkiGledeNaSocket[socket.id]+' (zasebno)' + ': ' +  besedilo});
     socket.emit('zasSporOdgovor',{
       oseba : 'sender' ,
       besedilo : besedilo,
       ime: zamenjajXSS(vzdevkiGledeNaSocket[sockI])
       //'(zasebno za '+vzdevkiGledeNaSocket[sockI]+')' + ': ' +  besedilo});
     });
    }
  })
  
}

function zamenjajXSS(besedilo){
    var tmpBesedilo=besedilo.replace(/</g,"&lt");
    tmpBesedilo=tmpBesedilo.replace(/>/g,"&gt");
    return tmpBesedilo;
}
function zamenjajSmeske(besedilo){
  var sporocilo=besedilo.replace(/;\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">");
  sporocilo=sporocilo.replace(/:\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">");
  sporocilo=sporocilo.replace(/\(y\)/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">");
  sporocilo=sporocilo.replace(/:\*/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">");
  sporocilo=sporocilo.replace(/:\(/g,"<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">");
  return sporocilo;
}



